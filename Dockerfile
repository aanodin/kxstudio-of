FROM ubuntu:17.10
MAINTAINER Alexander Anodin <aanodin@gmail.com>

ENV TERM linux

RUN apt-get -yqq update
# prevents errors on following package installations
RUN apt-get -yqq install apt-utils 
RUN apt-get -yqq install dialog

RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula \
    select true | debconf-set-selections
RUN apt-get -yqq install libglibmm-2.4-1v5 ttf-mscorefonts-installer

# installing kxstudio
RUN apt-get -yqq install apt-transport-https software-properties-common wget \
 && rm -rf /var/lib/apt/lists/*

RUN wget https://launchpad.net/~kxstudio-debian/+archive/kxstudio/+files/kxstudio-repos_9.4.9~kxstudio1_all.deb

RUN dpkg -i kxstudio-repos_9.4.9~kxstudio1_all.deb && \
	dpkg --add-architecture i386 


RUN wget https://launchpad.net/~kxstudio-debian/+archive/kxstudio/+files/kxstudio-repos-gcc5_9.4.9~kxstudio1_all.deb
RUN dpkg -i kxstudio-repos-gcc5_9.4.9~kxstudio1_all.deb

RUN apt-get update && apt-get install -y lmms-vst-full:i386 lmms cadence \
kxstudio-meta-wine kxstudio-meta-audio kxstudio-meta-audio-plugins kxstudio-meta-restricted-extras kxstudio-default-settings \
 && rm -rf /var/lib/apt/lists/*


# install qt5
RUN apt-get -y install build-essential clang cmake gdb git gperf iputils-ping linux-tools-generic nano qt5-default valgrind wget
# install dependencies
RUN apt-get -yqq install libfontconfig1 libsndfile1 libopenal1 libglew2.0 libfreeimage3
# install jack
RUN DEBIAN_FRONTEND=noninteractive apt-get -yqq install jack-tools ant fftw3 qjackctl

# clean unnecessary installation files
RUN apt-get -yqq autoremove && apt-get -yqq autoclean && apt-get -yqq clean

# download and unpack openframeworks 0.9.8
WORKDIR /opt
RUN wget -q http://openframeworks.cc/versions/v0.9.8/of_v0.9.8_linux64_release.tar.gz
RUN tar -zxf of_v0.9.8_linux64_release.tar.gz
RUN mv of_v0.9.8_linux64_release of
RUN rm of_v0.9.8_linux64_release.tar.gz

# install openframeworks
WORKDIR of
RUN scripts/linux/ubuntu/install_dependencies.sh
RUN scripts/linux/compileOF.sh -j2
